import os
import io
import re
from datetime import datetime
import discord

from dotenv import load_dotenv

import log


# vars
load_dotenv()
WORKING_DIR=os.getenv("WORKING_DIR")
AT_INTERVENTION=os.getenv("AT_INTERVENTION")
AT_QUESTION=os.getenv("AT_QUESTION")
AT_D_ACCORD=os.getenv("AT_D_ACCORD")
AT_PAS_D_ACCORD=os.getenv("AT_PAS_D_ACCORD")
AT_HORS_SUJET=os.getenv("AT_HORS_SUJET")
AT_BREF=os.getenv("AT_BREF")
CHANNEL_LOG=int(os.getenv("CHANNEL_LOG"))
CHANNEL_ANNONCE=int(os.getenv("CHANNEL_ANNONCE"))
COLOR_LB=os.getenv("COLOR_LB")
COLOR_DISCUSSION=os.getenv("COLOR_DISCUSSION")
COLOR_DEBAT=os.getenv("COLOR_DEBAT")
COLOR_ENTRETIEN=os.getenv("COLOR_ENTRETIEN")
COLOR_MENU=os.getenv("COLOR_MENU")



async def menu(bot, message):
    embed = discord.Embed() 
    embed.title = "Menu d'aide"
    embed.description = f"""Chaque commande s'utilise en mentionnant le bot.

**-Affiche l'aide.**
`help`

**-Affiche les emojis utilisé pour faciliter les échanges.**
`emotes` ou `emoji` ou `emoticones`

**-Affiche le tutoriel pour activer le push-to-talk ou appuyer-pour-parler.**
`pushtotalk` ou `aidevocale` ou `tutoptt`

**-Affiche les informations de la chaîne Thinkerview.**
`thinkerview` ou `tkv`

**-Affiche la dernière vidéo de Thinkerview.**
`derniere_video` ou `tkv_last` ou `tkv_derniere`

**-Affiche dans un fil les actus des [X] derniers jours. valeur à 1 par défaut.**
`actu [X]`

---------------------
**Modérateur seulement**

**-Désactive le bot dans un salon ACTU/VEILLE**
`source-ignore <CHANNEL>`
**-Active le bot dans un salon ACTU/VEILLE**
`source-check <CHANNEL>`

    """
    
    embed.color = int(COLOR_MENU, 16)


    file_path = WORKING_DIR + "assets/logo.png"
    with open(file_path, "rb") as file:
        image = file.read()
    logo = "attachment://logo_tkv.png"
    embed.set_thumbnail(url="attachment://logo.png")
    
    await message.channel.send(embed=embed)
    


async def help_mod(bot): 
    print("commandes d'aide modérateur.")


async def emoticones(bot, channel):

    file_path = WORKING_DIR + "assets/emoticones.jpg"
    log.log("file path = " + file_path)
    with open(file_path, "rb") as file:
        image = file.read()
    
    await channel.send(f""".\n<:at_intervention:{AT_INTERVENTION}> :at_intervention:
<:at_question:{AT_QUESTION}> :at_question:
<:at_d_accord:{AT_D_ACCORD}> :at_d_accord:
<:at_pas_d_accord:{AT_PAS_D_ACCORD}> :at_pas_d_accord:
<:at_hors_sujet:{AT_HORS_SUJET}> :at_hors_sujet:
<:at_bref:{AT_BREF}> :at_bref:""", file=discord.File(io.BytesIO(image), 'emoticones.jpg'))
    

"""
ID des émojis
serveur test :
:at_question:       data-id="1230960475068436640"
:at_intervention:   data-id="1230960336535031928">

serveur AT:
<:at_intervention:703353884164358224> :at_intervention:
<:at_question:703353967681208470> :at_question:
<:at_d_accord:703354001525309490> :at_d_accord:
<:at_pas_d_accord:703353927508426872> :at_pas_d_accord:
<:at_hors_sujet:703354052498423908> :at_hors_sujet:
<:at_bref:703354106789625957> :at_bref:
"""


async def pushtotalk(bot, channel):

    file_path = WORKING_DIR + "assets/pushtotalk.gif"
    log.log("file path = " + file_path)
    with open(file_path, "rb") as file:
        image = file.read()
    
    await channel.send("\n**Comment activer le push to talk :**", file=discord.File(io.BytesIO(image), 'pushtotalk.gif'))
  



async def publication(bot, message):
    log.log("Nouvelle publication en cours de rédaction...")
    log.log(message.content)

    titre = None
    contenu = None
    type_event = None
    intervenant = None
    quand = None
    enregistrement = None
    lien = None

    lines = message.content.split('\n')
    for line in lines:
        if re.match(r"^titre ?[=:]", line):
            titre = re.split(r"(?i)^titre\s*[=:]", line)[1].strip()[:99]  # Limite le titre à 99 caractères
        elif re.match(r"(?i)^(?:contenu|description)\s*[:=]", line):
            contenu = re.split(r"(?i)^(?:contenu|description)\s*[:=]", line)[1].strip()
        elif re.match(r"(?i)^type\s*[=:]", line):
            type_event = re.split(r"(?i)^type\s*[=:]", line)[1].strip()
        elif re.match(r"(?i)^(?:avec|intervenant|par)\s*[=:]", line):
            intervenant = re.split(r"(?i)^(?:avec|intervenant|par)\s*[=:]", line)[1].strip()
        elif re.match(r"(?i)^(?:quand|date|le)\s*[=:]", line):
            quand = re.split(r"(?i)^(?:quand|date|le)\s*[=:]", line)[1].strip()
        elif re.match(r"(?i)^(?:enregistrement|enr|rec|audio|podcast)\s*[=:]", line):
            enregistrement = re.split(r"(?i)^(?:enregistrement|enr|rec|audio|podcast)\s*[=:]", line)[1].strip()
        elif re.match(r"(?i)(?:lien|link|url)\s*[=:]", line):
            lien = re.split(r"(?i)(?:lien|link|url)\s*[=:]", line)[1].strip()
        else:
            if contenu:
                contenu += "\n" + line


    embed = discord.Embed() 

    if not titre:
        titre = "Publication."
    embed.title = titre

    if type_event:
        if re.match(r"(?i)(?:lb|livre blanc|exposé)", type_event):
            type_event = "Livre blanc"
            embed.color = int(COLOR_LB, 16)
        elif re.match(r"(?i)d[eé]bats?", type_event):
            type_event = "Débat"
            embed.color = int(COLOR_DEBAT, 16)
        elif re.match(r"(?i)(?:interview|entretien)", type_event):
            type_event = "Entretien"
            embed.color = int(COLOR_ENTRETIEN, 16)
        elif re.match(r"(?i)discussions?", type_event):
            type_event = "Discussion"
            embed.color = int(COLOR_DISCUSSION, 16)
        else:
            log.log("type d'événement inconnu : " + type_event)
    else:
        type_event = ""


    date_pattern = r"^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[0-2])\/(\d{2}|\d{4})\s*[àa]?\s*(0?[0-9]|1[0-9]|2[0-3])[h:]([0-5][0-9])$"
    if quand and re.match(date_pattern, quand):
        try:
            date_modifiee = re.sub(r'[aà]', '', quand)
            date_modifiee = re.sub('h', ':', date_modifiee)
            quand = datetime.strptime(date_modifiee, "%d/%m/%Y %H:%M").strftime("%A %d %B %Y à %Hh%M")
        except ValueError:
            log.log("Format de date invalide. Date modifiée avant lecture en datetime : " + date_modifiee)
            quand = "à venir" 
    else:
        log.log("Format de date ne correspond pas au format attendu")
        quand = "à venir"


    if enregistrement:
        enregistrement = enregistrement.lower()
        if enregistrement == "oui" or enregistrement == "yes" or enregistrement == "o" or enregistrement == "y":
            log.log("enregistrement : oui")
            enregistrement = "💾 Oui"
        else:
            log.log("enregistrement : non")
            enregistrement = "🔇 Non"


    if lien and not lien.startswith("https://"):
        log.log("Format de lien invalide")
        lien = None


    embed.description = f"{contenu}"

    embed.add_field(name="Avec", value=f"{intervenant}", inline = True)
    embed.add_field(name="\u200b", value="\u200b")
    embed.add_field(name="Type", value=f"{type_event}", inline = True)
    embed.add_field(name="Date", value=f"{quand}", inline = True)
    embed.add_field(name="\u200b", value="\u200b")
    embed.add_field(name="Enregistrement", value=f"{enregistrement}", inline = True)

 

    # gif => png ? pourquoi ? car discord ne supporte pas les gif dans ce genre d'embed. Et j'ai la flemme de convertir l'image en png.
    file_path = WORKING_DIR + "assets/logo_at.gif"
    with open(file_path, "rb") as file:
        image = file.read()
    logo = "attachment://logo.png"
    embed.set_thumbnail(url="attachment://logo.png")

    bot_channel = bot.get_channel(CHANNEL_LOG)
    await bot_channel.send(lien, embed=embed, file=discord.File(io.BytesIO(image), "logo.png"))







async def edit(bot, message):
    log.log("Edition de la publication")
    log.log(message.content)

    lines = message.content.splitlines()
    if len(lines) > 1:
        command = lines[0]
        post_id = command.split()[2].split("/")[-1]
        chan_id = command.split()[2].split("/")[-2]
        if post_id.isdigit():
            post_id = int(post_id)
        if chan_id.isdigit():
            chan_id = int(chan_id)
        else:
            return
        lines.pop(0)
    else:
        return

    log.log("message à éditer : " + str(post_id))
    log.log("salon du message : " + str(chan_id))

    message_channel = bot.get_channel(chan_id)
    publication = await message_channel.fetch_message(post_id)

    log.log("salon : " + message_channel.name)

    embed = publication.embeds[0]

    titre = None
    contenu = None
    type_event = None
    intervenant = None
    quand = None
    enregistrement = None
    lien = None



    for line in lines:
        if re.match(r"^titre ?[=:]", line):
            titre = re.split(r"(?i)^titre\s*[=:]", line)[1].strip()[:99]  # Limite le titre à 99 caractères
        elif re.match(r"(?i)^(?:contenu|description)\s*[:=]", line):
            contenu = re.split(r"(?i)^(?:contenu|description)\s*[:=]", line)[1].strip()
        elif re.match(r"(?i)^type\s*[=:]", line):
            type_event = re.split(r"(?i)^type\s*[=:]", line)[1].strip()
        elif re.match(r"(?i)^(?:avec|intervenant|par)\s*[=:]", line):
            intervenant = re.split(r"(?i)^(?:avec|intervenant|par)\s*[=:]", line)[1].strip()
        elif re.match(r"(?i)^(?:quand|date|le)\s*[=:]", line):
            quand = re.split(r"(?i)^(?:quand|date|le)\s*[=:]", line)[1].strip()
        elif re.match(r"(?i)^(?:enregistrement|enr|rec|audio|podcast)\s*[=:]", line):
            enregistrement = re.split(r"(?i)^(?:enregistrement|enr|rec|audio|podcast)\s*[=:]", line)[1].strip()
        elif re.match(r"(?i)(?:lien|link|url)\s*[=:]", line):
            lien = re.split(r"(?i)(?:lien|link|url)\s*[=:]", line)[1].strip()
        else:
            if contenu:
                contenu += "\n" + line

    if titre:
        embed.title = titre


    if intervenant:
        embed.set_field_at(0, name="Avec", value=f"{intervenant}", inline = True)

    if type_event:
        if re.match(r"(?i)(?:lb|livre blanc|exposé)", type_event):
            type_event = "Livre blanc"
            embed.color = int(COLOR_LB, 16)
        elif re.match(r"(?i)d[eé]bats?", type_event):
            type_event = "Débat"
            embed.color = int(COLOR_DEBAT, 16)
        elif re.match(r"(?i)(?:interview|entretien)", type_event):
            type_event = "Entretien"
            embed.color = int(COLOR_ENTRETIEN, 16)
        elif re.match(r"(?i)discussions?", type_event):
            type_event = "Discussion"
            embed.color = int(COLOR_DISCUSSION, 16)

        embed.set_field_at(2, name="Type", value=f"{type_event}", inline = True)




    date_pattern = r"^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[0-2])\/(\d{2}|\d{4})\s*[àa]?\s*(0?[0-9]|1[0-9]|2[0-3])[h:]([0-5][0-9])$"
    if quand and re.match(date_pattern, quand):
        try:
            date_modifiee = re.sub(r'[aà]', '', quand)
            date_modifiee = re.sub('h', ':', date_modifiee)
            quand = datetime.strptime(date_modifiee, "%d/%m/%Y %H:%M").strftime("%A %d %B %Y à %Hh%M")
            embed.set_field_at(3, name="Date", value=f"{quand}", inline = True)
        except ValueError:
            log.log("Format de date invalide")    


    if enregistrement:
        enregistrement = enregistrement.lower()
        if enregistrement == "oui" or enregistrement == "yes" or enregistrement == "o" or enregistrement == "y":
            log.log("Enregistrement : oui")
            enregistrement = "💾 Oui"
        else:
            log.log("Enregistrement : non")
            enregistrement = "🔇 Non"
        embed.set_field_at(5, name="Enregistrement", value=f"{enregistrement}", inline = True)


    if lien and not lien.startswith("https://"):
        log.log("Le lien n'est pas valide : " + lien)
        lien = None
    elif not lien and publication.content:
        lien = publication.content


    if not contenu:
        contenu = embed.description
    # nettoie les retour à la ligne en trop
    contenu = contenu.strip('\n')
    embed.description = f"{contenu}"


    file_path = WORKING_DIR + "assets/logo.png"
    with open(file_path, "rb") as file:
        image = file.read()
    logo = "attachment://logo_tkv.png"
    embed.set_thumbnail(url="attachment://logo.png")

    await publication.edit(content=lien, embed=embed)



async def publier(bot, message):
    log.log("Le post va être publier...")
    log.log(message)

    post_id = message.split("/")[-1]
    chan_id = message.split("/")[-2]
    if post_id.isdigit():
        post_id = int(post_id)
    if chan_id.isdigit():
        chan_id = int(chan_id)
    else:
        return

    log.log("message à publier : " + str(post_id))
    log.log("salon du message : " + str(chan_id))

    message_channel = bot.get_channel(chan_id)
    publication = await message_channel.fetch_message(post_id)

    log.log("salon : " + message_channel.name)
    
    log.log("sending...")
    channel_annonce = bot.get_channel(CHANNEL_ANNONCE)
    await channel_annonce.send(content="@everyone" + publication.content, embed=publication.embeds[0])
    log.log("publié !")




