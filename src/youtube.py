import os
import requests
import discord
import xml.etree.ElementTree as ET
import asyncio

from dotenv import load_dotenv
# from googleapiclient.discovery import build

import log
import thinkerview 


# vars
load_dotenv()
YT_CHAN_NAME=os.getenv("YT_CHAN_NAME")
YT_CHAN_URL=os.getenv("YT_CHAN_URL")
YT_CHAN_ID=os.getenv("YT_CHAN_ID")
YT_LATEST_VID_PATH=os.getenv("YT_LATEST_VID_PATH")
CHAN_ANNONCE=int(os.getenv("CHANNEL_ANNONCE"))
CHAN_TKV=int(os.getenv("CHANNEL_TKV"))
# YT_API_KEY=os.getenv("YT_API_KEY")



"""""""""
" using channel ID, get latest video uploaded data using xml feed
" return video data or, in case of error / no video found, the channel info
" @Args  : None
" Return : youtube_video_url, youtube_video_title, youtube_video_thumbnail_url
" alt Return : youtube_channel_url, youtube_channel_name, None
"""""""""
def get_latest_vid():
    yt_feed = "https://www.youtube.com/feeds/videos.xml?channel_id=" + YT_CHAN_ID
    # log.log("youtube feed link : " + yt_feed)

    html = requests.get(yt_feed)
    xml_data = html.content
    tree = ET.ElementTree(ET.fromstring(xml_data))
    entry = tree.find(".//{http://www.w3.org/2005/Atom}entry")

    if entry is not None:
        youtube_video_title = entry.find(".//{http://www.w3.org/2005/Atom}title").text.strip()
        video_id = entry.find(".//{http://www.youtube.com/xml/schemas/2015}videoId").text.strip()
        youtube_video_thumbnail_url = entry.find(".//{http://search.yahoo.com/mrss/}thumbnail").attrib['url']
        youtube_video_url = f"https://www.youtube.com/watch?v={video_id}"

        log.log("Titre :" + youtube_video_title)
        log.log("url de la vidéo :" + youtube_video_url)
        log.log("URL du thumbnail :" + youtube_video_thumbnail_url)
        # return YT_CHAN_URL, YT_CHAN_NAME, None
        return youtube_video_url, youtube_video_title, youtube_video_thumbnail_url
    else:
        log.log("Error : no video found in XML file.")
        return YT_CHAN_URL, YT_CHAN_NAME, None



def check_new_video():
    yt_feed = "https://www.youtube.com/feeds/videos.xml?channel_id=" + YT_CHAN_ID
    # log.log("youtube feed link : " + yt_feed)

    html = requests.get(yt_feed)
    xml_data = html.content
    tree = ET.ElementTree(ET.fromstring(xml_data))
    entry = tree.find(".//{http://www.w3.org/2005/Atom}entry")

    if entry is not None:
        video_id = entry.find(".//{http://www.youtube.com/xml/schemas/2015}videoId").text.strip()

        file_path = YT_LATEST_VID_PATH
        try:
            with open(file_path, "r") as file:
                latest_vid_id = file.read().strip()
            if latest_vid_id == video_id:
                return None
            else:
                with open(file_path, "w") as file:
                    file.write(video_id)
                return video_id
        except FileNotFoundError:
            with open(file_path, "w") as file:
                file.write(video_id)
            return video_id
    else:
        log.log("Error : no video found in XML file.")

"""""""""
" check if there is a new video every 60 seconds
" @Args  : None
" Return : None
"""""""""
async def timer_check_video(bot):
    while True:
        new_video = check_new_video()
        if new_video:
            log.log("Nouvelle vidéo thinkerview : " + new_video)
            channel_annonce = bot.get_channel(CHAN_TKV)
            tkv_info, image_file = thinkerview.latest_vid()
            await channel_annonce.send("@everyone, **Thinkerview est en direct !**", embed=tkv_info, file=image_file)

        await asyncio.sleep(60)










"""
# Function not useful because of API restrictions (only 100 requests per day)
def get_live_vid_API():
    # Crée une instance du client YouTube Data API

    youtube = build("youtube", "v3", developerKey=YT_API_KEY)

    # Get youtube channel ID with its name
    response = youtube.search().list(
        part="snippet",
        q=YT_CHAN_NAME,
        type="channel"
    ).execute()

    # check if ID was found
    if response.get("items") and len(response["items"]) > 0:
        channel_id = response["items"][0]["id"]["channelId"]
        log.log("channel ID found : " + channel_id)
    else:
        log.log("impossible to retrieve channel ID. Check that channel name is correct.")


    # get live videos of the channel
    response = youtube.search().list(
        part="snippet",
        channelId=channel_id,
        eventType="live",
        type="video"
    ).execute()

    live_video_url = "" 
    live_video_title = ""
    # Affiche les détails de la vidéo
    # print(response)
    if response.get("items") and len(response["items"]) > 0:
        # Récupère l'URL de la vidéo en direct du premier résultat de la liste
        live_video_url = f"https://www.youtube.com/watch?v={response['items'][0]['id']['videoId']}"
        live_video_title = response["items"][0]["snippet"]["title"]

        thumbnail_url = None

        if 'maxres' in response["items"][0]["snippet"]['thumbnails']:
            thumbnail_url = response["items"][0]["snippet"]['thumbnails']['maxres']['url']
        elif 'high' in response["items"][0]["snippet"]['thumbnails']:
            thumbnail_url = response["items"][0]["snippet"]['thumbnails']['high']['url']
        elif 'medium' in response["items"][0]["snippet"]['thumbnails']:
            thumbnail_url = response["items"][0]["snippet"]['thumbnails']['medium']['url']

        return live_video_url, live_video_title, thumbnail_url
"""








