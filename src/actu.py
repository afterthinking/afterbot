import os
import io
import re
import discord
import json

from dotenv import load_dotenv
from datetime import datetime, timedelta

import log


# vars
load_dotenv()
WORKING_DIR=os.getenv("WORKING_DIR")
CHANNEL_LOG=int(os.getenv("CHANNEL_LOG"))
CAT_ACTU=int(os.getenv("CAT_ACTU"))
CHANNEL_ACTU=int(os.getenv("CHANNEL_ACTU"))


async def check_actu(bot, message):
    if message.channel.category_id != CAT_ACTU:
        return
 
    conf_path = WORKING_DIR + "data/actu_chan_excl.json"
    with open(conf_path, "r") as f:
        try:
            excluded_channels = json.load(f)
        except:
            excluded_channels = []


    if message.channel.id in excluded_channels:
        return 

    if message.channel.type == discord.ChannelType.public_thread:
        return

    if message.is_system() or message.author.id == bot.user.id:
        return

    # si il y a une PJ ou une URL, on laisse le message et créé un fil
    # sinon, on prévient l'auteur et on supprime le message.
    url_pattern = r'https?://\S+'
    if len(message.attachments) > 0 or re.search(url_pattern, message.content):
        thread_name = "Discussion de l'actu"
        if message.content:
            thread_name = message.content[:20] + "..."
        elif message.attachments:
            thread_name = message.attachments[0].filename
        await message.create_thread(name=thread_name)
        return



    channel_admin = bot.get_channel(CHANNEL_LOG)
    channel_actu = bot.get_channel(CHANNEL_ACTU)
    log.log(f"{message.author.mention} poste sans source dans {message.channel.mention}.")
    await channel_admin.send(f"{message.author.mention} poste sans source dans {message.channel.mention}.")
    try:
        await message.author.send(f"{message.author.mention}, les salons de la catégorie {message.channel.category.name} sont réservés aux sources et non à la discussion. Veuillez créer un fil de discussion ou basculer dans le {channel_actu.mention}.")
        await message.author.send(f"Votre message :\n{message.content}")
    except discord.Forbidden:
        log.log(f"{message.author} n'accèpte pas les DM")
        await message.channel.send(f"{message.author.mention}, les salons de la catégorie {message.channel.category.name} sont réservés aux sources et non à la discussion. Veuillez créer un fil de discussion ou basculer dans le {channel_actu.mention}.")
        await message.channel.send(f"Votre message :\n{message.content}")

    try:
        await message.delete()
    except discord.NotFound:
        log.log("Message already deleted by another bot or user. Nothing to do.")
        pass
    except discord.Forbidden:
        log.log("Bot not authorized to delete this message :")
        log.log(message.content)
        pass




async def ignore_source(bot, channel_mention):

    if not channel_mention:
        return

    try:
        chan = bot.get_channel(int(channel_mention[2:-1]))
    except:
        log.log("Impossible to add a new channel to the ignore list. Unvalid data.")
        return

    if not isinstance(chan, discord.TextChannel):
        log.log("Impossible to add a new channel to the ignore list. Not a TextChannel.")
        return

    if chan.category_id != CAT_ACTU:
        return
 
    conf_path = WORKING_DIR + "data/actu_chan_excl.json"
    with open(conf_path, "r+") as f:
        try:
            excluded_channels = json.load(f)
        except:
            excluded_channels = []
        
        if chan.id in excluded_channels:
            log.log("Impossible to add a new channel to the ignore list. Already in the list.")
            return

        excluded_channels.append(chan.id)
        f.seek(0)
        json.dump(excluded_channels, f, indent=4)
        f.truncate()

        log.log("Channel added to the excluded list : " + chan.name + " ID is : " + str(chan.id))


async def check_source(bot, channel_mention):

    if not channel_mention:
        return

    try:
        chan = bot.get_channel(int(channel_mention[2:-1]))
    except:
        log.log("Impossible to add a new channel to the ignore list. Unvalid data.")
        return

    if not isinstance(chan, discord.TextChannel):
        log.log("Impossible to add a new channel to the ignore list. Not a TextChannel.")
        return

    conf_path = WORKING_DIR + "data/actu_chan_excl.json"
    with open(conf_path, "r+") as f:
        try:
            excluded_channels = json.load(f)
        except:
            excluded_channels = []
        
        if chan.id in excluded_channels:
            log.log("Channel is removed from the excluded list : " + chan.name + " ID is : " + str(chan.id))
            excluded_channels.remove(chan.id)
            f.seek(0)
            json.dump(excluded_channels, f, indent=4)
            f.truncate()




async def list_actu(bot, message, days):
    if not isinstance(message.channel, discord.TextChannel):
        return

    category = bot.get_channel(CAT_ACTU)
    if not category or not isinstance(category, discord.CategoryChannel):
        log.log("Cannot load the category " + str(CAT_ACTU))
        return
 
    conf_path = WORKING_DIR + "data/actu_chan_excl.json"
    with open(conf_path, "r") as f:
        try:
            excluded_channels = json.load(f)
        except:
            excluded_channels = []



    messages_with_attachments = []
    cutoff_date = datetime.utcnow() - timedelta(days=days)

    thread_name = "Actu des " + str(days) + " derniers jours"
    thread = await message.create_thread(name=thread_name)

    for channel in category.channels:
        if isinstance(channel, discord.TextChannel):
            if channel.id in excluded_channels:
                continue    
            await thread.send(f"**Message dans {channel.mention} :**")
            cur_date = None
            prev_date = None
            async for actu in channel.history(limit=None, after=cutoff_date):
                if actu.is_system() or actu.author.id == bot.user.id:
                    continue
                cur_date = actu.created_at.strftime("%A %d %B %Y")
                if str(cur_date) != str(prev_date):
                    await thread.send(f"**Le {cur_date} :**")
                    prev_date = cur_date
                if actu.attachments:
                    attachment_files = [await attachment.to_file() for attachment in actu.attachments]
                    await thread.send(f"{actu.jump_url} - {actu.content}", files=attachment_files)
                else:
                    await thread.send(f"{actu.jump_url} - {actu.content}")


