import datetime
import os

from dotenv import load_dotenv

load_dotenv()


# vars
log_dir=str(os.getenv("LOG_DIR"))
log_file=log_dir + "after_bot.log"


# prepare :
# Function to prepare the afterbot log file 
# rename previous log file with current date to have a clean log file
def log_archive():
    cur_date=str(datetime.date.today())
    cur_time=str(datetime.datetime.now().time())
    dest = log_dir + cur_date + "_" + cur_time + "_" + "afterbot.log"
    os.rename(log_file, dest)


# plog(txt) :
# print the txt log with current date into the log_file.
def log(txt):
    cur_date=str(datetime.date.today())
    cur_time=str(datetime.datetime.now().time())

    f = open(log_file, "a")
    f.write("[" + cur_date + " - "  + cur_time  + "] : " + txt + "\n")
    f.close()


