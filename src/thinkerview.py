# libraries
import os
import io
import discord

import log
import youtube
from dotenv import load_dotenv


# vars
load_dotenv()
WORKING_DIR=os.getenv("WORKING_DIR")
YT_CHAN_NAME=os.getenv("YT_CHAN_NAME")
YT_CHAN_URL=os.getenv("YT_CHAN_URL")
TWITCH_URL=os.getenv("TWITCH_URL")
CAPTAINFACT_URL=os.getenv("CAPTAINFACT_URL")
TIPEEE_URL=os.getenv("TIPEEE_URL")
DISCORD_URL=os.getenv("DISCORD_URL")


"""""""""
" Get thinkerview youtube data and format them to an embbed ready to be displayed on discord
" @Args  : None 
" return : Embed object
"""""""""
def latest_vid():
    # get latest video
    url, title, thumbnail = youtube.get_latest_vid()

    embed = discord.Embed() 
    embed.title = f"{title}"
    embed.description = f"""> ThinkerView est un groupe indépendant issu d'internet, très différent de la plus part des think-tanks qui restent inféodés à des partis politiques ou des intérêts privés.\n
    """
    """
    __***Pour regarder le direct :***__
    - [youtube]({url})\n- [twitch]({TWITCH_URL})\n
    __***Pour poser des questions durant le direct :***__
    - [discord officiel]({DISCORD_URL})\n
    __***Sourcez, vérifiez les faits en direct :***__
    - [Captain Fact]({CAPTAINFACT_URL})\n
    __***Pour faire un don :***__
    - [tipeee]({TIPEEE_URL})"""
    """
    """
    embed.add_field(name="Regarder le direct", value=f"- [youtube]({url})\n- [twitch]({TWITCH_URL})", inline = True)
    embed.add_field(name="Poser des questions", value=f"- [discord]({DISCORD_URL})", inline = True)
    embed.add_field(name="\u200b", value="\u200b")
    embed.add_field(name="Vérifier les faits", value=f"- [captain fact]({CAPTAINFACT_URL})", inline = True)
    embed.add_field(name="Soutenir la chaîne", value=f"- [tipeee]({TIPEEE_URL})", inline = True)
    embed.add_field(name="\u200b", value="\u200b")

    
    embed.color = int('0x000000', 16)


    file_path = WORKING_DIR + "assets/logo.png"
    with open(file_path, "rb") as file:
        image = file.read()
    logo = "attachment://logo.png"
    embed.set_thumbnail(url="attachment://logo.png")
    
    if isinstance(thumbnail, str):
        embed.set_image(url=thumbnail)

    return embed, discord.File(io.BytesIO(image), "logo.png")



"""""""""
" Get thinkerview youtube data and format them to an embbed ready to be displayed on discord
" @Args  : None 
" return : Embed object
"""""""""
def info():

    embed = discord.Embed() 
    embed.title = f"{YT_CHAN_NAME}"
    embed.description = f"""> ThinkerView est un groupe indépendant issu d'internet, très différent de la plus part des think-tanks qui restent inféodés à des partis politiques ou des intérêts privés.\n
    """
    """
    __***Pour regarder le direct :***__
    - [youtube]({YT_CHAN_URL})\n- [twitch]({TWITCH_URL})\n
    __***Pour poser des questions durant le direct :***__
    - [discord officiel]({DISCORD_URL})\n
    __***Sourcez, vérifiez les faits en direct :***__
    - [Captain Fact]({CAPTAINFACT_URL})\n
    __***Pour faire un don :***__
    - [tipeee]({TIPEEE_URL})
    """

    embed.add_field(name="La chaîne thinkerview", value=f"- [youtube]({YT_CHAN_URL})\n- [twitch]({TWITCH_URL})", inline = True)
    embed.add_field(name="Poser des questions", value=f"- [discord]({DISCORD_URL})", inline = True)
    embed.add_field(name="\u200b", value="\u200b")
    embed.add_field(name="Vérifier les faits", value=f"- [captain fact]({CAPTAINFACT_URL})", inline = True)
    embed.add_field(name="Soutenir la chaîne", value=f"- [tipeee]({TIPEEE_URL})", inline = True)
    embed.add_field(name="\u200b", value="\u200b")

    
    embed.color = int('0x000000', 16)


    file_path = WORKING_DIR + "assets/logo.png"
    with open(file_path, "rb") as file:
        image = file.read()
    logo = "attachment://logo_tkv.png"
    embed.set_thumbnail(url="attachment://logo.png")
    
    return embed, discord.File(io.BytesIO(image), "logo.png")











