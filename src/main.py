# libraries
import discord
import os
import asyncio
import locale


import log
import thinkerview 
import youtube
import aides
import audio
import actu

from discord.ext import commands
from dotenv import load_dotenv


# vars
load_dotenv()
token=os.getenv("TOKEN")
CHANNEL_LOG=int(os.getenv("CHANNEL_LOG"))
CAT_ACTU=int(os.getenv("CAT_ACTU"))

intents = discord.Intents.all()
print(intents)


bot = commands.Bot(command_prefix=".", intents=intents)


"""
" Init function.
" Load the blacklist and tell admins the bot is not active and log bot informations
"""
@bot.event
async def on_ready():
    log.log("Afterbot is starting.")
    log.log("Bot name : " + bot.user.name)
    log.log("Bot ID : " + str(bot.user.id))

    channel_admin = bot.get_channel(CHANNEL_LOG)
    await channel_admin.send(bot.user.name + " démarre")


    locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')

    # #10 video en direct
    asyncio.create_task(youtube.timer_check_video(bot))


"""
" Check all messages if a word is within the blacklist
"
"   @arg1: message received by the API
"""
@bot.event
async def on_message(message):

    if message.author.id == bot.user.id or message.author.bot or message.is_system() or len(message.content) == 0:
        return
    

    if message.channel.category_id == CAT_ACTU:
        await actu.check_actu(bot, message)
        # nb : avec ce code ici et ce return en particulier, ça empêche l'usage de toute commande du bot dans des salons et fils de la catégorie ACTU/VEILLE
        return



    sentence_words = message.content.split()
    first_word = sentence_words[0]
    command = "".join(sentence_words[1:]).lower()

    # Pour utiliser le bot, il faut le mentionner avec la commande à effectuer.
    if bot.user.mention in first_word:
        if not command:
            return

        # Commandes accessibles à tous.
        if command == "help":
            await aides.menu(bot, message)
            return

        elif command == "emotes" or command == "emoji" or command == "emoticones" :
            # #3 Émoticones.
            await aides.emoticones(bot, message.channel)

        elif command == "pushtotalk" or command == "appuyerpourparler" or command == "aidevocale" or command == "tutoptt":
            # #4 Aide push-to-talk.
            await aides.pushtotalk(bot, message.channel)

        elif command == "thinkerview" or command == "tkv":
            # #8bis basic info 
            tkv_info, image_file = thinkerview.info()
            await message.channel.send(embed=tkv_info, file=image_file)

        elif command == "derniere_video" or command == "tkv_last" or command == "tkv_derniere":
             # #8 latest vid
            tkv_info, image_file = thinkerview.latest_vid()
            await message.channel.send(embed=tkv_info, file=image_file)

        elif sentence_words[1] == "actu" :
            # si actu est demandé avec un paramètre foireux, il affiche sur une journée
            if len(sentence_words) <= 2:
                await actu.list_actu(bot, message, 1)
                return
            elif not sentence_words[2].isdigit():
                await actu.list_actu(bot, message, 1)
                return
            elif int(sentence_words[2]) < 1:
                await actu.list_actu(bot, message, 1)
                return
            else:
                await actu.list_actu(bot, message, int(sentence_words[2]))
                return


        

        # Commandes accessibles uniquement à la modération.
        if "🦊Modération" in [role.name for role in message.author.roles] or message.author.guild_permissions.administrator:
            if command == "help":
                # TODO: déterminer si on affiche toutes les commandes ou si on a besoin d'avoir cet espace commandes modérateurs
                await aides.menu(bot, message)
                return

            if sentence_words[1] == "source-ignore" :
                await actu.ignore_source(bot, "".join(sentence_words[2:]))
                return

            if sentence_words[1] == "source-check" :
                await actu.check_source(bot, "".join(sentence_words[2:]))
                return

            if sentence_words[1] == "publication":
                await aides.publication(bot, message)
                return

            if sentence_words[1] == "edit" or sentence_words[1] == "editer" :
                await aides.edit(bot, message)
                return

            if sentence_words[1] == "publier":
                await aides.publier(bot, sentence_words[2])
                return



            # #9 Audio recording
            """
            Ce code sert à tester l'enregistrement audio avec le bot. Il est a priori fonctionnel, mais souffre de divers problèmes. Voir l'issue #9 pour plus d'infos.
            """
            """
            if message.author.voice:
                voice_channel = message.author.voice.channel 
                if message.content == "!join":
                    await audio.join(voice_channel, message.channel)
                if message.content == "!quit":
                    await audio.quit(voice_channel, message.channel)
                if message.content == "!record":
                    await audio.record(voice_channel, message.channel)
                    # asyncio.create_task(audio.record(voice_channel, message.channel))
                if message.content == "!stop":
                    await audio.stop(voice_channel, message.channel)
                if message.content == "!pause":
                    await audio.pause(voice_channel)
                if message.content == "!resume":
                    await audio.resume(voice_channel)
            """

@bot.event
async def on_message_edit(before, after):

    if before.author.id == bot.user.id:
        return



if __name__ == "__main__":
    bot.run(token)


