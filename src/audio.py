import os
import platform
import io
import subprocess
import datetime
import discord
from discord.ext import listening

from dotenv import load_dotenv

import log


# vars
load_dotenv()
WORKING_DIR=os.getenv("WORKING_DIR")
CHANNEL_LOG=int(os.getenv("CHANNEL_LOG"))

process_pool = None

"""
" fonction permettant au bot de rejoindre le vocal de l'utilisateur
"
"""
async def join(voice_channel, text_channel):
    if voice_channel.guild.voice_client:
        await text_channel.send("Le bot est déjà en vocal")
        log.log("Le bot est déjà en vocal")
    else:
        await voice_channel.connect(cls=listening.VoiceClient)
        await text_channel.send("Le bot a rejoint le vocal")
        log.log("Le bot a rejoint le vocal")


async def quit(voice_channel, text_channel):
    global process_pool
    if voice_channel.guild.voice_client:
        if voice_channel.guild.voice_client.is_listen_receiving():
            log.log("Arrêt de l'enregistrement")
            await stop(voice_channel, text_channel)
        await text_channel.send("Le bot quitte le vocal")
        log.log("Le bot quitte le vocal")
        await voice_channel.guild.voice_client.disconnect()
        if process_pool:
            process_pool.cleanup_processes()
    else:
        await text_channel.send("Le bot n'est pas dans un salon vocal")
        log.log("Le bot n'est pas dans un salon vocal")





# The key word arguments passed in the listen function MUST have the same name.
# You could alternatively do on_listen_finish(sink, exc, channel, ...) because exc is always passed
# regardless of if it's None or not.
# note : je ne sais toujours pas tout à fait comment ça fonctionne
async def on_listen_finish(sink: listening.AudioFileSink, exc=None, channel=None):
    # Convert the raw recorded audio to its chosen file type
    # kwargs can be specified to convert_files, which will be specified to each AudioFile.convert call
    # here, the stdout and stderr kwargs go to asyncio.create_subprocess_exec for ffmpeg
    log.log("Conversion des fichiers en wav.")

    count=0
    cur_date=str(datetime.date.today())
    cur_time=str(datetime.datetime.now().time())
    dest = ""
    file_list=[]
    file_creation_datetime=[]
    print("covert in progress...")
    for file in sink.output_files.values():
        dest = "audio_" + cur_date + "_" + cur_time + "_" + file.user.name + "_" + str(count)
        file_list.append(dest)
        count += 1
        creation_datetime = datetime.datetime.fromtimestamp(os.stat(file.path).st_atime)
        file_creation_datetime.append(creation_datetime)
        await file.convert(dest)
    print("done...")
    print("files :")
    print(file_list)
    print(file_creation_datetime)
    # await sink.convert_files(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    # Raise any exceptions that may have occurred
    if exc is not None:
        raise exc



async def record(voice_channel, text_channel):
    global process_pool
    voice_client = voice_channel.guild.voice_client

    if not voice_client:
        print("le bot n'est pas en vocal")
        await text_channel.send("Le bot n'est pas en vocal.")
        return
    if voice_client.is_listen_receiving():
        print("enregistrement déjà en cours.")
        await text_channel.send("Enregistrement déjà en cours.")
        return
    if voice_client.is_listen_cleaning():
        print("En train de nettoyer l'écoute, recommencer dans quelques secondes")
        await text_channel.send("En train de nettoyer l'écoute, recommencer dans quelques secondes")

    process_pool = listening.AudioProcessPool(1)
    FILE_FORMATS = {"mp3": listening.MP3AudioFile, "wav": listening.WaveAudioFile}

    voice_client.listen (
        listening.AudioFileSink(FILE_FORMATS["wav"], "data/audio"),
        process_pool,
        after=on_listen_finish,
        channel=text_channel
    )
    print("started recording")





async def stop(voice_channel, text_channel):
    voice_client = voice_channel.guild.voice_client
    if not voice_client:
        print("le bot n'est pas en vocal")
        await text_channel.send("Le bot n'est pas en vocal.")
        return
    if voice_client.is_listen_receiving():
        vc = voice_channel.guild.voice_client
        vc.stop_listening()
        await text_channel.send("Fin de l'enregistrement.")
        return
    else:
        await text_channel.send("Aucun enregistrement en cours.")
        return
        




# TODO : vérifier le fonctionnement et le code des fonction PAUSE et RESUME
async def pause(interaction: discord.Interaction):
    if not await is_in_guild(interaction):
        return
    # Make sure we're currently in vc and recording.
    if interaction.guild.voice_client is None or not (await get_vc(interaction)).is_listen_receiving():
        return await interaction.response.send_message("Not currently recording.")
    vc = interaction.guild.voice_client
    # Make sure we're not already paused
    if vc.is_listening_paused():
        return await interaction.response.send_message("Recording is already paused.")
    # Pause the recording and then change deafen state to indicate so. Note the
    # deafen state does not actually prevent the bot from receiving audio.
    vc.pause_listening()
    await change_deafen_state(vc, True)
    await interaction.response.send_message("Recording paused.")




async def resume(interaction: discord.Interaction):
    if not await is_in_guild(interaction):
        return
    # Make sure we're currently in vc and recording.
    if interaction.guild.voice_client is None or not (await get_vc(interaction)).is_listen_receiving():
        return await interaction.response.send_message("Not currently recording.")
    vc = interaction.guild.voice_client
    # Make sure we're paused
    if not vc.is_listening_paused():
        return await interaction.response.send_message("Recording is already resumed.")
    # Resume the recording and then change the deafen state to indicate so.
    vc.resume_listening()
    await change_deafen_state(vc, False)
    await interaction.response.send_message("Recording resumed.")




