# AfterBot


## But 

l'AfterBot a pour but de fournir un outil de modération au serveur de l'After Thinking ainsi que des outils à la disposition des membres.


## Fonctionnalités 

- ✅ Vérifier qu'un message posté dans l'ACTU/VEILLE (hors demande d'article) soit faite avec une source (fichier, lien...)
- Aide à la mise en page d'une annonce.
- ✅ Poster rapidement les emoticônes
- ✅ Poster le gif comment activer le push-to-talk
- Création d'un fil question/réponse (+ lien vers celui-ci quand on lui demande)
- Afficher la liste des demande de parole avec commande pour nettoyer la liste (FIFO)
- ❌ Capacité à enregistrer l'audio sur demande
- ✅ Afficher l'ensemble des actus postés les X derniers jours (sur demande)
- ✅ Poster à la demande les infos thinkerview (sans ping)
- ✅ Vérifier si une nouvelle vidéo thinkerview est dispo (avec ping)


## Comment participer ?

Contactez moi via gitlab ou directement sur le discord.
Le projet a une base « propre » sur laquelle il sera facile d'ajouter son travail, n'hésitez pas à faire un merge request.


## Installation

Aller dans le répertoire d'installation de  votre choix
```
git clone https://gitlab.com/afterthinking/afterbot
```
Vérifiez que les permissions soient bonnes. 

Allez dans le répertoire du projet.
```
cd afterbot 
```

Créez un environnement virtuel
```
python -m venv .venv
```
Activez-le
```
source .venv/bin/activate
```

Installez les bibliothèques requises, listées dans le fichier *requirements.txt* : 
```
pip install -r requirements.txt
```

Créez un fichier `.env` et ajoutez les informations de configuration suivantes

*nota bene : cette liste est temporaire avec potentiellement des infos inutiles. Elle sera susceptible de changer à l'avenir.*
```
TOKEN=
CHANNEL_LOG=
CHANNEL_ANNONCE=
CHANNEL_TKV=
CHANNEL_ACTU=
CAT_ACTU=
LOG_DIR=
WORKING_DIR=
YT_CHAN_NAME=Thinkerview
YT_CHAN_URL=https://www.youtube.com/@thinkerview
YT_CHAN_ID=UCQgWpmt02UtJkyO32HGUASQ
YT_LATEST_VID_PATH=
TWITCH_URL=https://www.twitch.tv/thinkerview 
CAPTAINFACT_URL=https://captainfact.io/s/Thinkerview 
TIPEEE_URL=https://fr.tipeee.com/thinkerview
DISCORD_URL=https://discord.gg/Ey3pBWV 
AT_INTERVENTION=703353884164358224
AT_QUESTION=703353967681208470
AT_D_ACCORD=703354001525309490
AT_PAS_D_ACCORD=703353927508426872
AT_HORS_SUJET=703354052498423908
AT_BREF=703354106789625957
COLOR_LB=0xFFFFFF
COLOR_DISCUSSION=0x3333AA
COLOR_DEBAT=0xAA3333
COLOR_ENTRETIEN=0x33AA33
COLOR_MENU=0x4466AA

```
- TOKEN : TOKEN du bot tel que fournit par votre console discord.
- SERVER_GUILD : ID du serveur.
- CHANNEL_LOG : salon de log pour les admins sur les actions entreprises par le bot.
- CHANNEL_ANNONCE : salon public des annonces du serveur.
- CHANNEL_TKV : salon public des annonces spécifiques à Thinkerview.
- LOG_DIR : chemin vers le dossier de log. nb : sera supprimée à terme.
- WORKING_DIR : chemin vers le dossier où se trouve le README du projet.
- AT_* : ID de chaque émoji accessible via la console de browser. Les ID ci-dessus sont ceux du serveur de l'AT.

Créer le répertoire des logs
```
mkdir log
```

Créer le répertoire des données stockées
```
mkdir data
```

Un fichier sera nécessaire pour garder en mémoire les salons sans vérification de sources. Créer le fichier actu_chan_excl.json :
```
touch data/actu_chan_excl.json
```

Si vous voulez expérimenter l'audio, il faudra également créer le dossier data/audio :
```
mkdir data/audio
```

Tout est configuré, le bot peut maintenant s'exécuter :
```
python src/main.py & 
```

Pour faciliter la relance du bot, un script peut être fait :
```
cat > afterbot.sh << EOF
#!/bin/sh
        
pid=`ps -ef | grep -i "afterbot_dev" | head -n 1 | tr -s ' ' | cut -d' ' -f2`           
echo "Suppression du pid $pid"
kill -9 $pid

source /srv/services/after_thinking/afterbot_dev/.venv/bin/activate
pip install -r /srv/services/after_thinking/afterbot_dev/requirements

python -u /srv/services/after_thinking/afterbot_dev/src/main.py > log/exec.log 2>&1 &

deactivate
EOF
```


## Usage

// TODO


## Auteur

Zilot


## License

Licence GNU GPLv3
